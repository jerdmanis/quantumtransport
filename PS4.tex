\documentclass[11pt]{article}  % list options between brackets
\usepackage{fullpage}

\usepackage{epsfig,amsmath,amssymb, color}

\renewcommand{\thesubsection}{\thesection.\alph{subsection}}

\newcommand{\ket}[1]{\left\vert#1\right\rangle}
\newcommand{\bra}[1]{\left\langle#1\right\vert}
\newcommand{\braket}[2]{\left< #1 \vphantom{#2}\, \right\vert\left.\!\vphantom{#1} #2 \right>  }
\newcommand{\projector}[2]{\left\vert#1\right\rangle\left\langle#2\right\vert}
\newcommand{\sandwich}[3]{\left< #1 \vphantom{#2 #3} \right| #2 \left|\vphantom{#1 #2} #3 \right>}
\newcommand{\ddt}{\frac{d}{dt}}
\newcommand{\pdd}[1]{\frac{\partial}{\partial#1}}
\newcommand{\numop}[1]{\displaystyle #1^{\dagger} #1}
\newcommand{\vari}[1]{\frac{\delta}{\delta#1}}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\kv}[1]{\mathbf{k}_{#1}}
\newcommand{\vecb}[1]{\vec{\textbf{#1}}}
\newcommand{\prim}{^{^{\prime}}}

\begin{document}


\title{Quantum Transport - Problem Set 4}
\date{\today}
\maketitle
\pagenumbering{arabic}

\section{Weak Localization in 1D}

Consider the conductance of a thin wire connected to macroscopic leads. The weak localization correction to the conductance in one dimension is given by the expression
\begin{equation*}
  \delta G \approx - G_Q \frac{D}{L^2} \int_0^\infty dt \int dx P(x,x,t)
\end{equation*}
where $D$ is the diffusion coefficient and L is the length of the wire. $P(x,x',t)$ is the probability that an electron from $x$ travels to $x'$ in the time interlaval $t$. The integrated probability density $W (x, x ) = \int_{0}^\infty dt P (x, x', t)$ gives the average time it takes an electron at position $x$ to travel to $x'$. Time $L^2/D$ is the average time it takes an electron to diffuse through the whole system. $P(x, x', t)$ obeys the diffusion equation:
\begin{equation*}
  \left(
    \frac{\partial}{\partial t} - D \frac{\partial^2}{\partial x^2}
  \right) P(x,x',t) = \delta( x - x') \delta(t)
\end{equation*}
The boundary conditions are $P(0,x',t)=P(L,x',t)=0$, arising from the assumption that if an electrin reaches the ends of the wire, it will diffuse into the leads, never returning back. In addition, $P (x, x' , t = \infty) = 0$, i.e. given sufficient time, the electron will leave the wire.

\begin{enumerate}
\item Integrate the diffusion equation over $t$ and give the relation you find for $W (x, x')$.
\item Find the general solution for $W (x, x')$, when $x \neq x'$. (homogenous solution)
\item There is a discontinuity in $\partial W (x, x')/ \partial x$ at $x = x'$. Apply the boundary conditions to find the general solution for $W (x, x')$ when $x < x'$ and when $x>x'$.
\item Using the fact that $W (x, x )$ must be continuous, i.e. $W (x' - 0, x' ) = W (x' + 0, x' )$, write down the full solution for $W (x, x')$.
\item Calculate the order of magnitude of $|\delta G|$.
%\item Compare the conductance corection with the one of thin wire.  
\end{enumerate}

\section{Resonant Qubit Manipulation}

We describe a qubit by the two-component wave function:
\begin{align*}
  \psi(t) = \left(
  \begin{array}[c]{c}
    \psi_\uparrow(t) \\
    \psi_\downarrow(t)
  \end{array}
  \right),
&&
    \textnormal{with} && \psi(0) = | \downarrow \rangle = \left(
     \begin{array}[c]{c}
       0 \\
       1
     \end{array}
  \right)
\end{align*}

The qubit experiences a field in the $z$-direction and an oscillating field in the $x$-direction. The Hamiltonian describing the evolution of the qubit is:
\begin{align*}
  \hat H(t) = \hbar B_z \hat \sigma_z + \hbar B_x \hat \sigma_x \cos(\Omega t),
\end{align*}
where $\hat \sigma$ are Pauli matricies:
\begin{align*}
  \hat \sigma_x = \left(
  \begin{array}{cc}
    0 & 1 \\
    1 & 0
  \end{array}
         \right)
      &&
         \hat \sigma_y = \left(
  \begin{array}{cc}
    0 & -i \\
    i & 0
  \end{array}
         \right)
      &&
  \hat \sigma_z = \left(
  \begin{array}{cc}
    1 & 0 \\
    0 & -1
  \end{array}
         \right)
\end{align*}

\begin{enumerate}

\item Show explicitly that
  \begin{equation*}
    \exp (i B_z \hat \sigma_z t) = \cos (B_z t) + i \hat \sigma_z \sin (B_z t)
  \end{equation*}
  Hint: Use the Taylor expansion
\item Let us change to a rotating frame of reference: $\phi(t) = \exp(iB_z \hat \sigma_z t) \psi(t)$ . Show that the equivalent of the Schrödinger equation $i \hbar \dot \psi  = \hat H \psi$ in terms of $\phi(t)$ is:
  \begin{equation*}
    i \dot \phi(t) = B_x \cos (\Omega t) \left(
      \begin{array}{cc}
        0 & \exp (2 i B_z t) \\
        \exp (-2i B_z t) & 0
      \end{array}
    \right)  \phi(t)
  \end{equation*}

\item Consider the case of resonant driving $\Omega = 2B_z$ . Simplify the expression of $i \dot \phi (t)$. 

  We observe that the (transformed) Hamiltonian has a part that is constant in time and a part that oscillated with frequency $4B_z$ . If we are interested in dynamics of the qubit that is much slower than the period of the oscillation, the rapidly oscillating term will average out and we may neglect it (Rotating Wave Approximation, or RWA). The effective Hamiltonian becomes $\hat H_{eff} = \hbar B_x \hat \sigma_x$ : the qubit experiences a static field in the x-direction. Let us study the dynamics without employing the RWA.

\item Let us keep the rapidly oscillating term. We may use the coupled differential equations obtained for the case of resonant driving at task 3 to calculate $\phi_\uparrow (t)$ and $\phi_\downarrow(t)$. We do this by using first order perturbation theory in $B_x$ . When does this approximation break down? 

\end{enumerate}

\section{Controlled-NOT Gate}
Spin is the canonical example of a two level system. In 1998, Loss and DiVicenzo proposed a method to use the spin of electrons confined in quantum dots for quantum computations (see page 445 of the textbook). Let us assume that an external magnetic field is present (oriented along the $z$-direction). The qubit Hamiltonian (describing the dynamics of the electron spin) is:
\begin{equation*}
  \hat H = - \mu_e B \hat \sigma_z
\end{equation*}
where $\mu_e$ is the magnetic moment of the electron. The basis we employed is:
\begin{align*}
  | \downarrow \rangle = \left(
    \begin{array}[c]{c}
      0 \\
      1
    \end{array}
  \right)
  &&
 \textnormal{and}
  &&
    | \uparrow \rangle = \left(
    \begin{array}[c]{c}
      1 \\
      0
    \end{array}
  \right)
\end{align*}
In order to perform universal quantum computations, we need a method to tune the interaction between two qubits. In this proposal, the interaction could be turned on and off by tuning the tunneling barrier between two dots. When the tunneling barrier is low, the electrons are brought close together and experience the exchange interaction characterized by the Hamiltonian:
\begin{equation*}
  \hat H_{int} = J \hat \sigma_z^{(1)} \hat \sigma_z^{(2)}
\end{equation*}
which is called an Ising type interaction. The Ising-type interaction is sufficient to perform the CNOT operation, which, in combination with single qubit operations, enables universal quantum computations.

The CNOT gate is an operation on two qubits. It has the effect of changing the state of one (target) qubit, only when the other (control) qubit is in the $| \uparrow \rangle$ state. In matrix representation (equation 5.4 on page 379):

\begin{equation*}
  CNOT = \left(
    \begin{array}{cccc}
      1&0&0&0 \\
      0&1&0&0 \\
      0&0&0&1 \\
      0&0&1&0 
    \end{array}
  \right)
\end{equation*}
where the basis used is ordered as: $| \downarrow \downarrow \rangle$,$| \downarrow \uparrow \rangle$ $| \uparrow \downarrow \rangle$ and $| \uparrow \uparrow \rangle$. Qubit (1) is the control and qubit (2) is the target qubit. Our goal is to describe the realization of a CNOT gate using the Ising type interaction.

\begin{enumerate}
\item The effect of turning on $\hat H_{int}$ for a time $\tau$ is that the relative phase between two qubit states are shifted as follows: states $| \uparrow \uparrow \rangle$ and $| \downarrow \downarrow \rangle$ gain phase factor $\exp(iJ \tau /\hbar)$ while states $| \downarrow \uparrow \rangle$ and  $| \uparrow \downarrow \rangle$ gain phase factor $\exp(-iJ \tau /\hbar)$. Write down the 4x4 matrix representation of the two-qubit gate $G_{int}$ that is built by turning on the interaction for a time $\tau = \pi/4 \cdot \hbar/J$. Multiply by the phase factor $\exp(i\pi/4)$ to obtain a diagonal matrix with eigenvalues $i$ and $1$.
\item To transform the two qubit gate $G_{int}$ into the CNOT gate, we use single qubit manipulation of the first and second qubit. In the space of one qubit, a rotation by $\pi$ about the $z$-axis has the form:
  \begin{equation*}
    R_z = \left(
      \begin{array}{cc}
        -i & 0 \\
        0 & i
      \end{array}
    \right)
  \end{equation*}
Write down the $4x4$ representation of $R_z$ performed only on qubit $1$ and $R_z$ performed only on qubit $2$.
\item Use $G_{int}$, $R_z^{(1)}$ and $R_z^{(2)}$ to construct the controlled-$Z$ gate:
  \begin{equation*}
    C_Z = \left(
    \begin{array}{cccc}
      1&0&0&0 \\
      0&1&0&0 \\
      0&0&1&0 \\
      0&0&0&-1 
    \end{array}
  \right)
\end{equation*}
\item The Hadamard gate is a single qubit gate that has the action
  \begin{equation*}
    H = \frac{1}{\sqrt{2}} \left(
      \begin{array}{cc}
        1 & 1 \\
        1 & -1 
      \end{array}
    \right)
  \end{equation*}
  Write down the $4x4$ representation of $H^{(2)}$ , i.e. gate H acting on qubit 2 and performing no action on qubit 1.
\item Use the equality: $\hat \sigma_x = H \hat \sigma_z H$ to transform the controlled-$Z$ gate into a controlled-NOT gate.
  \end{enumerate}
  
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
