\documentclass[11pt]{article}  % list options between brackets
\usepackage{fullpage}

\usepackage{epsfig,amsmath,amssymb, color}

% \renewcommand{\thesubsection}{\thesection.\alph{subsection}}

% \newcommand{\rmnum}[1]{\romannumeral #1}
% \newcommand{\Rmnum}[1]{\expandafter\@slowromancap\romannumeral #1@}

\newcommand{\ket}[1]{\left\vert#1\right\rangle}
\newcommand{\bra}[1]{\left\langle#1\right\vert}
\newcommand{\braket}[2]{\left< #1 \vphantom{#2}\, \right\vert\left.\!\vphantom{#1} #2 \right>  }
\newcommand{\projector}[2]{\left\vert#1\right\rangle\left\langle#2\right\vert}
\newcommand{\sandwich}[3]{\left< #1 \vphantom{#2 #3} \right| #2 \left|\vphantom{#1 #2} #3 \right>}
\newcommand{\ddt}{\frac{d}{dt}}
\newcommand{\pdd}[1]{\frac{\partial}{\partial#1}}
\newcommand{\numop}[1]{\displaystyle #1^{\dagger} #1}
\newcommand{\vari}[1]{\frac{\delta}{\delta#1}}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\kv}[1]{\mathbf{k}_{#1}}
\newcommand{\vecb}[1]{\vec{\textbf{#1}}}
\newcommand{\prim}{^{^{\prime}}}

\begin{document}

\title{Quantum Transport - Problem Set \uppercase\expandafter{\romannumeral 1}}
\date{\today}
\author{J. Erdmanis and Y. V. Nazarov}
\maketitle
\pagenumbering{arabic}

\section*{Charge transport statistics}

We discussed in the lecture \#1 that the characteristic function $\Lambda(\chi)$ can be used to calculate cumulants (average fluctuations of the random variable to a certain power), thus describing the full statistics of electron transport through nanostructures. This problem challenges you to perform such calculations. The expression of $\Lambda (\chi)$ is provided by the Levitov formula (Eq. 1.54 of the textbook)
\begin{eqnarray}
  \ln \Lambda (\chi) = 2_s \Delta t \int \frac{d E}{2 \pi \hbar} \sum_p \ln \left\{
  1 + T_p (e^{i \chi} - 1) f_L(E) [1 - f_R(E)] \right. \nonumber \\ 
  \left. + T_p (e^{-i \chi} - 1) f_R(E) [1 - f_L(E)]
  \right\} 
   \nonumber
\end{eqnarray}
for the case of a multichannel conductor, two electron reservoirs and finite temperature. The n-th cumulant of the charge transfer $Q$ can be calculated as:
\begin{align}
  \langle \langle Q^n \rangle \rangle = e^n \left. \frac{\partial^n \ln \Lambda (\chi)}{\partial (i \chi)^n} \right|_{\chi=0} \nonumber
\end{align}
\begin{enumerate}
\item Derive the expression for $\langle Q \rangle$ to prove that $\langle I \rangle = \langle Q \rangle / \Delta t$ agrees with the Landauer formula (Eq. 1.37 of the textbook).
  \begin{eqnarray}
    I = \frac{2_s e}{2 \pi} \int_0^\infty dE \sum_p T_p [f_{L}(E) - f_R(E)] \nonumber
  \end{eqnarray}
  \item Derive the expression for the zero frequency noise $S(0) = 2 \langle \langle Q^2 \rangle \rangle / \Delta t$\footnote{This is a definition of noise for this exercise.}. 
  \item Find $S(0)$ at equilibrium (zero bias, $f_L = f_R$ , \emph{Nyquist-Johnson noise}) and show that it is proportional to the conductance and the temperature.
  \item Assume now the opposite limit: finite bias and low temperature $eV \gg k_B T$. Find the expression for $S(0)$ (shot noise).
  \item Use the previous results to find the expression for Fano factor $F = S(0)/2e\langle I \rangle$ and check the result with Eq. 1.64 of the textbook.
  \item Consider a case where the conductance of a nanostructure is measured to be $G=0.5 G_Q$ ($G_Q = 2e^2 /h$ is the quantum of conductance). The measured noise is characterized by Fano factor $F = 0.59$. Assuming that only two channels contribute to conductance, calculate the transmission eigenvalues of both channels. %{\bf Hint}: use the previous result (Eq. 1.64 of the textbook)
\end{enumerate}

% \section{Fano factors}

\section*{Aharonov Bohm ring}
\begin{center}
  \includegraphics[width=0.8 \textwidth]{figures/PS2_pb1.pdf}   
\end{center}

Consider transmission probability through a ring connected to two reservoirs via two fully symmetric beam splitters
\begin{align}
       \hat s_{1} = \hat s_{2} = \hat s 
  =
  \frac{1}{3}
  \left(
         \begin{array}{ccc}
           1 + 2 e^{i \phi} & 1 - e^{i \phi}  & 1 - e^{i \phi} \\
           1 - e^{i \phi} & 1 + 2 e^{i \phi} & 1 - e^{i \phi} \\
           1 - e^{i \phi} & 1 - e^{i \phi} & 1 + 2 e^{i \phi} 
         \end{array}
                   \right)
                   =
                   \left(
         \begin{array}{ccc}
           r & t  & t \\
           t & r & t \\
           t & t & r 
         \end{array}
         \right).
  \nonumber
\end{align}
The ring consists of two arms (see the figure), each supporting one transport channel. When electron crosses an arm it acquires a phase which consists of two components: dynamical $\chi_{1,2}$ and magnetic $\phi_{1,2}$ phases. The magnetic phases are related via the magnetic flux flowing through the ring $\phi_1 + \phi_2 = π\Phi/\Phi_0$ ($\Phi_0$ is the magnetic flux quantum). (This exercise is based on the book {\bf Exercise 1.11.})

\begin{enumerate}
\item Calculate contribution to the transmission amplitude through the system that comes from two trajectories that undergo no internal reflections.

\item Let's now focus on all trajectories in which an electron from terminal 1 first enters the upper arm. Consider amplitudes $t_{uu}^{(n)}$ ($t_{du}^{(n)}$) for an electron that enters the left beamsplitter, goes to the upper arm first and exits at the right beamsplitter coming from the upper (lower) arm while making $n$ round trips\footnote{In other words $2n$ scatterings from the beamsplitters where the electron returns to upper or lower arm.}. For example, $t_{ud}^{(0)} = 0$. In the task 1 you have already used $t_{uu}^{(0)}$. Give expressions for $t_{uu}^{(1)}$, $t_{ud}^{(1)}$ and $t_{uu}^{(2)}$. 
%  Let's now denote with amplitudes $t_u^{(n)}$ ($t_d^{(n)}$) for electron to cross system through upper (lower) arm by making $2n$ reflections. 
%({\bf Hint:} Consider all possible trajectories which end in right beamsplitter thorugh upper or lower chanel and sum up their amplitudes).
\item The amplitudes $t_{uu}^{(n)}$, $t_{ud}^{(n)}$ can be expressed in terms of $t_{uu}^{(n-1)}$, $t_{ud}^{(n-1)}$. Derive these recursion formulas (don't forget initial condition). {\bf Hint.} How would you express $t_{uu}^{(2)}$ in terms of $t_{uu}^{(1)}$ and $t_{ud}^{(1)}$?
  
\item Consider now the sum of all amplitudes $t_{uu}=\sum_{n=0}^\infty t_{uu}^{(n)}$ and $t_{ud} = \sum_{n=0}^\infty t_{ud}^{(n)}$. Derive a linear system of two equations which determines these transmission amplitudes. {\bf Hint:}
    \begin{eqnarray}
      \left(
      \begin{array}{c}
        t_{uu} \\
        t_{ud} 
      \end{array}
      \right) =
      \sum_{n=0}^\infty
      \left(
      \begin{array}{c}
        t_{uu}^{(n)} \\
        t_{ud}^{(n)} 
      \end{array}
      \right)
      = \sum_{n=0}^\infty {\boldmath Q}^n
      \left(
      \begin{array}{c}
        t_{uu}^{(0)} \\
        t_{ud}^{(0)}   
      \end{array}
      \right)
      =
      \left(
      \begin{array}{c}
        t_{uu}^{(0)} \\
        t_{ud}^{(0)} 
      \end{array}
      \right)
      +
      {\boldmath Q } \left(
      \begin{array}{c}
        t_{uu} \\
        t_{ud} 
      \end{array}
      \right)
    \end{eqnarray}
    
    where $Q$ is $2x2$ matrix to be derived.
    
  \item Now consider all trajectories that first enter the lower branch from the terminal 1. Derive similar equations for $t_{du}$ and $t_{dd}$. {\bf Hint:} You only need to change indicies in the previous result.

  \item Now find the total transmission amplitude $t_{tot}$ and the corresponding probability. {\bf Hint:} $t_{tot} = t_{uu} + t_{ud} + t_{du} + t_{dd}$.
  \item \textbf{Bonus:} Calculate the total transmission amplitude between left and right arm by writting consitency relations for propgating waves. See more details from the last question of the next exercise.

\end{enumerate}

\section*{Partial Probability Amplitudes and Interference}
\begin{center}
  \includegraphics[width=0.8 \textwidth]{figures/PS1_pb3.pdf}  
\end{center}

Consider a fully symetric beam splitter with a scattering matrix $\hat{s}_0$ that is connected to three leads by identical one-chanel scattereres with a scattering matrix $\hat{s}$. 
\begin{align}
  \hat s = \left(
  \begin{array}{cc}
    r & t' \\
    t & r' 
  \end{array}
        \right)
      &&
         \hat s_0 = \left(
         \begin{array}{ccc}
           \hat{s}_{11} & \hat{s}_{12}  & \hat{s}_{13} \\
           \hat{s}_{21} & \hat{s}_{22} & \hat{s}_{23} \\
           \hat{s}_{31} & \hat{s}_{32} & \hat{s}_{33} 
         \end{array}
                                         \right)
                                         %= % \left(
         % \begin{array}{ccc}
         %   r & t  & t \\
         %   t & r & t \\
         %   t & t & r 
         % \end{array}
         %           \right)
                   =
%                     \frac{1}{3}
  % \left(
  %        \begin{array}{ccc}
  %          1 + 2 e^{i \phi} & 1 - e^{i \phi}  & 1 - e^{i \phi} \\
  %          1 - e^{i \phi} & 1 + 2 e^{i \phi} & 1 - e^{i \phi} \\
  %          1 - e^{i \phi} & 1 - e^{i \phi} & 1 + 2 e^{i \phi} 
  %        \end{array}
  %                                            \right)
  \left(
         \begin{array}{ccc}
           r_0 & t_0  & t_0 \\
           t_0 & r_0 & t_0 \\
           t_0 & t_0 & r_0 
         \end{array}
                   \right)
  \nonumber
\end{align}
In addition, the phase shifts are accumulated betwwen the scatterers and the splitter (see Fig 6). Our main task in this exercise is to find the transmission amplitude $t_{1\to 2}$ from the branch 1 to the branch 2.

\begin{enumerate}
\item Consider the scattering in the branch $1$ seperately (see Figure a). Calculate transmission amplitude of  $t_1$ for one electron coming from the left and reflection amplitude $r_1$ for one electron coming from the right. Write down the scattering matrix $\hat{s}_1$ of this branch.
 
\item Now let's consider the full structure and for the moment concentrate on trajectories that are not reflected in branches 1,2. Calcualte the simplest trajectory of this kind which does not visit the branch 3.


%  For making our task simpler we will ignore reflections in the branch 1 and in the branch 2. Calculate the amplitude of trajectory to start at branch 1 go through beamsplitter end exit at branch 2.

\item More complex trajectories of this kind start at the branch 1, goes through the beamsplitter to the  branch 3 and reflects several times bouncing between the beamspliteter and the scatterer 3. Calculate the total transmission amplitude of these trajectories including trajectory from task 2.


%  then goes back through beamsplitter to branch 2 where it exits. Assuming that electron can be reflected multiple times between beamsplitter and scatterer in the branch 3, calculate transmission amplitude through this path. {\bf Hint:} Sum up all trajectory amplitudes.

  
%   two simplest trajectories which enters the branch 1 and exit at branch 2.
  
%   {\bf Hint} There are two paths linking branch 1 and 2: path A: electron is transmitted from scatterer 1 to scatterer 2 via the beam splitter, path B: electron is transmitted from scatterer 1 to 3 and then from 3 to 2. Give the probability amplitude corresponding to the two paths including only trajectories without reflections in the beam splitter or in scatterers 1 and 2.

% \item  The two simplest trajectories considered in the task 2 can be complicated by all the possible ways in which an electron can move through the nanostructure and return back to the same point. Give the amplitudes of theese complex trajectories. 

%   {\bf Hint} To compute amplitudes coming from all the complicated trajectories, we must include reflections. Focus for now on the path that does not involve branch $3$. Let us consider trajectories which only involve one transmission across the beam splitter, but any numbers of reflections within branches $1$ and $2$. Calculate the total amplitude by writing down the partial amplitude for $N_1$ reflections back and forth in branch 1 and $N_2$ similar reflections in branch 2 and then, summing over all $N_1$ and $N_2$. 

% \item We have exhausted all ways of arriving to the same point involving only reflections. It is also possible for one electron to return to the same point due to transmissions via the beam splitter. There are in total 5 such trajectories. Draw them and compute their partial amplitudes (do not consider internal reflections yet). 

%   {\bf Hint}  For example: starting in branch $1$ the electron can be transmitted between branches $1$ and $2$, reflected in branch $2$. transmitted back from 2 to 1 and reflected once more in branch 1. Calculate the partial amplitude for this process. Calculate the similar amplitudes involving reflection back and forth between branches 1 and 3 and branches 3 and 2. Another was in which an electron can be brought to the same point (we consider only single transmissions through the beam splitter in any
%   direction) is via going once around the structure: either from branches $1 \to 2 \to 3 \to 1$ or from branches $1 \to 3 \to 2 \to 1$.

% \item Now enrich all 5 calculated trajectories above with all possible reflections between $\hat{s}$ and $\hat{s}_0$ similarly as you did in task 3. 

% \item Calculate the total probability amplitude $t_0$ for an electron to return to the same point while moving through the beam splitter not more than once in each direction, by summing up the amplitudes you calculated above. Use this to recalculate amplitudes in question 3 and calculate the transmission amplitude $t_{1 \to 2}$.

%  \pagebreak
  
\item Let us calculate the transmission amplitudes using a different method. Denote by $a_\alpha$ ($b_\alpha$) the amplitudes of waves propagating to (from) the beamsplitter in a branch $\alpha$. Express the amplitudes $t_{1\to 2}$ and $a_2$ through $b_2$. Finally, find the relations between $a_\alpha$ and $b_{\alpha}$ originating from the presence of the beam splitter. Derive the resulting system to obtain the amplitudes $b_\alpha$ and corresponding formula for finding $t_{1\to 2}$.

  % going out of the structure via scatterer 2. Compare your results. 

  
  % $c_3$ will denote the unknown wave amplitude going out of branch 3. The incoming amplitude is simply 1. Find the relations between the outgoing amplitude we are seeking (out of scatterer 2), $a_2$ and $b_2$ . Also, find the relations between $a_1$ and $b_1$ and between $a_3$ and $b_3$ originating from the presence of the two terminal scatterers. 
  
\end{enumerate}



\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
